# Installation

## Create Database
    Craft needs a Database to be created prior to setup

## Create Project
    `composer create-project -s dev bezar/craft-cms-template <PATH>`
    `cd <PATH>

## Setup

    `./craft setup`
    
## install the packages
    
    `composer install`
    
    `yarn install`
    
## install craft plugins

    `./craft install/plugin twigpack`

## sync the the db with config/project.yaml
    
    `./craft project-config/sync`